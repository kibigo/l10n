// 🌐🦁 L十n ∷ deps.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

export {
  construct,
  defineOwnProperties,
  freeze,
  getPropertyValue,
  hasOwnProperty,
  hasProperty,
  isArraylikeObject,
  isExtensible,
  LazyLoader,
  map,
  ordinaryHasInstance,
  pop,
  reduce,
  setPropertyValue,
  stringReplace,
  stripLeadingAndTrailingASCIIWhitespace,
  toObject,
  type,
} from "https://gitlab.com/kibigo/Pisces/-/raw/0.1.2/mod.js";
