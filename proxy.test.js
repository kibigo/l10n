// 🌐🦁 L十n ∷ Proxy.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import {
  assert,
  assertEquals,
  assertStrictEquals,
  assertThrows,
  describe,
  it,
} from "./dev-deps.js";
import LocalizationProxy from "./proxy.js";

describe("L十n.Proxy", () => {
  it("[[Construct]] constructs a L十n.Proxy.%Object%", () => {
    const strings = new LocalizationProxy({});
    assertStrictEquals(
      strings[Symbol.toStringTag],
      "Localization Proxy Object",
    );
  });

  it("[[Construct]] has an empty `[L十n.Proxy.path]`", () => {
    assertEquals(
      new LocalizationProxy({})[LocalizationProxy.path],
      [],
    );
  });
});

describe("L十n.Proxy.%Object%", () => {
  const strings = new LocalizationProxy({
    current: {
      bad: "failure",
      deeply: {
        nested: {
          current: {
            key: "deeply nested current key",
          },
        },
      },
      fallback: undefined,
      key: "current key",
      [Symbol.toPrimitive]: "failure",
    },
    default: {
      bad: {},
      deeply: {
        nested: {
          current: {
            key: "deeply nested default current key",
          },
          default: {
            key: "deeply nested default key",
          },
        },
      },
      fallback: "fallback to default key",
      key: "default key",
      [Symbol.toPrimitive]: "failure",
    },
    [LocalizationProxy.currentLanguage]: "current",
    [LocalizationProxy.defaultLanguage]: "default",
  });

  describe("::resolvePath", () => {
    it("[[Call]] resolves the path", () => {
      assertStrictEquals(
        strings.resolvePath("deeply", "nested", "current", "key"),
        "deeply nested current key",
      );
      assertStrictEquals(
        strings.resolvePath("deeply", "nested", "default", "key"),
        "deeply nested default key",
      );
    });
  });

  describe("~⸺", () => {
    it("[[Get]] pulls off the prototype chain if the localization is not present", () => {
      assertStrictEquals(
        strings.resolvePath,
        Object.getPrototypeOf(strings).resolvePath,
      );
      assertStrictEquals(strings.not_present_anywhere, undefined);
    });

    it("[[Get]] prefers the current language", () => {
      assertStrictEquals(strings.key, "current key");
    });

    it("[[Get]] falls back to the default language", () => {
      assertStrictEquals(strings.fallback, "fallback to default key");
    });

    it("[[Get]] ignores symbols", () => {
      assertStrictEquals(strings[Symbol.toPrimitive], void {});
    });

    it("[[Get]] accesses a deeply nested key", () => {
      assertStrictEquals(
        strings.deeply.nested.current.key,
        "deeply nested current key",
      );
      assertStrictEquals(
        strings.deeply.nested.default.key,
        "deeply nested default key",
      );
    });

    it("[[Get]] throws an error for a type mismatch between languages", () => {
      assertThrows(() => strings.bad);
    });

    it("[[Has]] returns true if the name exists on the localization object", () => {
      assert("key" in strings);
    });

    it("[[Has]] pulls off the prototype chain if the localization is not present", () => {
      assert("constructor" in strings);
    });

    it("[[Set]] throws if the name exists on the localization object", () => {
      assertThrows(() => strings.key = "failure");
    });

    it("[[Set]] can set own properties", () => {
      const emptyStrings = new LocalizationProxy({});
      emptyStrings.success = "success";
      assert(Object.hasOwn(emptyStrings, "success"));
      assertStrictEquals(emptyStrings.success, "success");
      emptyStrings.success = "still success";
      assertStrictEquals(emptyStrings.success, "still success");
    });
  });
});
