# 🌐🦁 L十n

Easy Localization Strings.

## Usage

```js
import * as L十n from "./mod.js";

const strings = new L十n.Proxy({
  // Often it is convenient to load these from an external file via a
  // JSON import.
  en: {
    MyNamespace: {
      foo: "Foo localization",
      hello: "Hello $1",
    },
  },

  // The preferred language to use when translating strings.
  [L十n.Proxy.currentLanguage]: "en",

  // The fallback language if a string in the preferred language isn’t
  // found.
  [L十n.Proxy.defaultLanguage]: "en",
});

class MyNamespace {
  // Instead of defining this getter, you could just make MyNamespace
  // directly inherit from L十n.Object.
  static get l十n() {
    return L十n.Object.l十n;
  }

  // `[L十n.strings]` is used to get the localization strings for an
  // object. Here, it is a path on the localization proxy established
  // above.
  static get [L十n.strings]() {
    return strings.MyNamespace;
  }
}

MyNamespace.l十n`foo`; // "Foo localization"
MyNamespace.l十n`hello ${"world"}`; // "Hello world"
MyNamespace.l十n`bar`; // "bar"
```
