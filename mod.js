// 🌐🦁 L十n ∷ mod.js
// ====================================================================
//
// Copyright © 2021–2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import strings from "./Localizations/mod.js";
import LocalizationObject from "./object.js";
import LocalizationProxy from "./proxy.js";
import { strings as stringsSymbol } from "./symbols.js";

export {
  LocalizationObject as Object,
  LocalizationProxy as Proxy,
  stringsSymbol as strings,
};

/**
 * Namespace for l十n localizations
 */
class L十n extends LocalizationObject {
  static [stringsSymbol] = strings.L十n;
}

/**
 * A class decorator for localizable objects.
 *
 * This decorator defines a `l十n` static method on the provided class
 * which can be used to tag template literals and provide their
 * localized values.
 */
export const localized = (_value, { kind, addInitializer }) => {
  if (kind == "class") {
    // This decorator is being used to decorate a class.
    addInitializer(function () {
      Object.defineProperty(this, "l十n", {
        configurable: true,
        enumerable: false,
        get() {
          return LocalizationObject.l十n;
        },
      });
    });
  } else {
    // This decorator is being used to decorate something other than a
    // class.
    throw new TypeError(L十n.l十n`nonclass decorator`);
  }
};

/**
 * A class decorator for localizable objects with a provided
 * localizations object.
 *
 * This is an extension to `@localized` which defines the
 * `L十n.strings` property to pull from a localization proxy on
 * the provided object according to the name of the class.
 */
export const localizedWith = (localizations) => (_value, context) => {
  localized(_value, context);
  context.addInitializer(function () {
    const proxyObject = new LocalizationProxy(localizations);
    Object.defineProperty(this, stringsSymbol, {
      configurable: true,
      enumerable: false,
      get() {
        return proxyObject[this.name];
      },
    });
  });
};
